import React, { PropTypes } from 'react'
import cuid from 'cuid'
import { Card, CardHeader, CardText } from 'material-ui/Card'
import QuizChoice from './QuizChoice'

const Quiz = ({ quiz }) => {
  return (
    <Card>
      <CardHeader title={quiz.question} actAsExpander={true} showExpandableButton={true} />
      <CardText expandable={true}>
        <form>
        { quiz.choices.map(choice => <QuizChoice key={cuid()} qid={quiz.id} choice={choice} />) }
        </form>
      </CardText>
    </Card>
  )
}

Quiz.propTypes = {
  quiz: PropTypes.shape({
    id: PropTypes.number.isRequired,
    question: PropTypes.string.isRequired,
    choices: PropTypes.arrayOf(PropTypes.shape({
      num: PropTypes.number.isRequired,
      answer: PropTypes.string.isRequired
    }))
  })
}

export default Quiz;
