import React, { Component } from 'react'
import { connect } from 'react-redux'
import { render } from 'react-dom'
import { addQuiz } from './QuizActions'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import { addAnswer } from './QuizWidgetActions'
import Divider from 'material-ui/Divider'
import DropDownMenu from 'material-ui/DropDownMenu'
import MenuItem from 'material-ui/MenuItem'
import 'material-design-lite/src/typography/_typography.scss'

class QuizWidget extends Component {
  constructor(props) {
    super(props)
    this.handleAddQuiz = this.handleAddQuiz.bind(this)
    this.handleAddMoreAnswer = this.handleAddMoreAnswer.bind(this)
    this.handleQuestionTextField = this.handleQuestionTextField.bind(this)
    this.handleTotalChoiceMenu = this.handleTotalChoiceMenu.bind(this)
    this.handleAnswerTextField = this.handleAnswerTextField.bind(this)
    this.totalAnswer = this.props.totalAnswer
    this.answers = []
    for(let i=1; i<=this.totalAnswer; i++) {
      this.answers.push(this.createAnswer(i))
    }
    this.state = {
      question: '',
      choices: [
        {num: 1, answer: ''},
        {num: 2, answer: ''},
        {num: 3, answer: ''},
        {num: 4, answer: ''},
      ],
      totalChoices: 1
    };
  }

  handleAddMoreAnswer = ()  => {
    this.answers.push(this.createAnswer(++this.totalAnswer))
    let choices = this.state.choices
    choices.push({
      num: this.totalAnswer,
      answer: ''
    })
    this.setState({
      choices: choices
    })
    const { dispatch } = this.props
    dispatch(addAnswer())
  }

  handleAddQuiz = () => {
    const { dispatch } = this.props
    dispatch(addQuiz(this.state))
  }
  handleTotalChoiceMenu = (e, index, value) => {
    this.setState({
      totalChoices: value
    });
  }
  handleQuestionTextField = e => {
    this.setState({
      question: this.refs.question.getValue(),
    })
  }
  handleAnswerTextField = e => {
    this.setState({
      choices: this.state.choices.map((choice, index) => {
        if(index == parseInt(e.target.id) -1) {
          choice.answer = e.target.value
        }
        return choice
      })
    })
  }
  createAnswer(id) {
    let text = `Answer ${id}`
    let aid = `${id}`
    return <TextField floatingLabelText={text} fullWidth={true} id={aid} onChange={this.handleAnswerTextField}/>
  }
  addQuestion() {

  }
  render() {
    return (
      <div>
        <TextField
          floatingLabelText="Question" multiLine={true} rows={3} fullWidth={true} onChange={this.handleQuestionTextField} ref='question'
        />
        <div id="answers">{this.answers}</div>
        <span className="mdl-typography--title">Total Answer: </span><DropDownMenu value={this.state.totalChoices} onChange={this.handleTotalChoiceMenu}>
          <MenuItem value={1} primaryText="1" />
          <MenuItem value={2} primaryText="2" />
          <MenuItem value={3} primaryText="3" />
        </DropDownMenu>
        <Divider />
        <FlatButton
          label='Add More Answer Input'
          primary={true}
          onClick={this.handleAddMoreAnswer}
        />
        <FlatButton
          label='Add Quiz'
          primary={true}
          onClick={this.handleAddQuiz}
        />
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    totalAnswer: state.widget.totalAnswer
  }
}

export default connect(mapStateToProps)(QuizWidget)
