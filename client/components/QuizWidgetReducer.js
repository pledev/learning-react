import { ADD_ANSWER } from './QuizWidgetActions'

const initialState = {
  totalAnswer: 4
}

const QuizWidgetReducer = (state = initialState, action) => {
  switch(action.type) {
    case ADD_ANSWER:
      return {
        totalAnswer: ++state.totalAnswer
      }
    default:
      return state
  }
}

export default QuizWidgetReducer
