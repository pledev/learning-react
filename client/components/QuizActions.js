export const CHOOSE_ANSWER = 'CHOOSE_ANSWER'
export const SHOW_ADD_WIDGET = 'SHOW_ADD_WIDGET'
export const ADD_QUIZ = 'ADD_QUIZ'

export const addQuiz = (quiz) => {
  return {
    type: ADD_QUIZ,
    quiz
  }
}
export const chooseAnswer = (qid, num) => {
  return {
    type: CHOOSE_ANSWER,
    qid,
    num
  }
}
export const showAddWidget = (show) => {
  return {
    type: SHOW_ADD_WIDGET,
    show
  }
}
