import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import Checkbox from 'material-ui/Checkbox'
import { chooseAnswer } from './QuizActions'

const QuizChoice = ({qid, choice, onCheck}) => {
  return (
    <Checkbox label={choice.answer} onCheck={onCheck} checked={choice.isChecked}/>
  )
}

QuizChoice.propTypes = {
  choice: PropTypes.shape({
    num: PropTypes.number.isRequired,
    answer: PropTypes.string.isRequired
  })
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onCheck: () => {
      dispatch(chooseAnswer(ownProps.qid, ownProps.choice.num))
    }
  }
}

export default connect(null, mapDispatchToProps)(QuizChoice)
