import { ADD_QUIZ, CHOOSE_ANSWER, SHOW_ADD_WIDGET } from './QuizActions'

const initialState = {
  quizs: [
    {
      id: 1,
      question: 'Suppose that you have an application whose behavior depends on the environment variable BAR. Which of the following command lines may be used in a bash shell to configure the application?',
      choices: [
        { num: 1, answer: 'export $BAR=baz; echo $BAR'},
        { num: 2, answer: 'set BAR=baz'},
        { num: 3, answer: 'BAR=baz ; export BAR'},
        { num: 4, answer: 'echo $BAR=baz'},
        { num: 5, answer: 'declare -x BAR=baz'},
        { num: 6, answer: 'echo BAR=baz'}
      ],
      totalChoices: 2,
      currentChoices: []
    },
    {
      id:  2,
      question: 'Which of the following commands can be used to assure that a file "myfile" exists?',
      choices: [
        { num: 1, answer: 'cp myfile /dev/null'},
        { num: 2, answer: 'touch myfile'},
        { num: 3, answer: 'create myfile'},
        { num: 4, answer: 'mkfile myfile'}
      ],
      totalChoices: 1,
      currentChoices: []
    }
  ],
  isShow: false
}

const QuizReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_QUIZ:
      return {
        quizs: [
          Object.assign({}, action.quiz, {currentChoices: []}),
          ...state.quizs
        ],
        isShow: state.isShow
      }
    case CHOOSE_ANSWER:
      return {
        quizs: state.quizs.map(quiz => {
          if(quiz.id === action.qid) {
            let checkChosen = quiz.currentChoices.indexOf(action.num)
            if(checkChosen > -1) {
              quiz.currentChoices.splice(checkChosen, 1)
            } else {
              if(quiz.currentChoices.length === quiz.totalChoices) {
                quiz.currentChoices.shift()
              }
              quiz.currentChoices.push(action.num)
            }

          }
          return quiz
        }),
        isShow: state.isShow
      }
    case SHOW_ADD_WIDGET:
      return Object.assign({}, { quizs: state.quizs }, { isShow: action.show })
    default:
      return state
  }
}

export const getQuizs = (state) => {
  return state.quiz.quizs.map(quiz => {
    quiz.choices = quiz.choices.map(choice => {
      let isChecked = false
      if(quiz.currentChoices.indexOf(choice.num) > -1) {
        isChecked = true
      }
      return Object.assign({}, choice, { isChecked: isChecked })
    })
    return quiz
  })
}
export default QuizReducer
