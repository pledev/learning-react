export const ADD_ANSWER = 'ADD_ANSWER'

export const addAnswer = () => {
  return {
    type: ADD_ANSWER
  }
}
