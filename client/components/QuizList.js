import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import Quiz from './Quiz'
import FlatButton from 'material-ui/FlatButton'
import { getQuizs } from './QuizReducer'
import QuizWidget from './QuizWidget'
import { showAddWidget } from './QuizActions'

class QuizList extends Component {
  constructor(props) {
    super(props)
    this.handleOpenWidget = this.handleOpenWidget.bind(this)
  }
  handleOpenWidget() {
    const { dispatch, isShow } = this.props
    dispatch(showAddWidget(!isShow))
  }
  render() {
    const { quizs, isShow } = this.props
    const label = isShow ? 'Close Widget' : 'Open Widget'
    return (
      <div>
        <FlatButton label={label} primary={true} onClick={this.handleOpenWidget}/>
        { isShow ? <QuizWidget /> : null }
        {quizs.map((quiz, index) => (
          <Quiz key={index} quiz={quiz}/>
        ))}
      </div>
    )
  }
}

QuizList.propTypes = {
  quizs: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    question: PropTypes.string.isRequired,
    choices: PropTypes.arrayOf(PropTypes.shape({
      num: PropTypes.number.isRequired,
      answer: PropTypes.string.isRequired
    })).isRequired
  })).isRequired,
  isShow: PropTypes.bool.isRequired,
}

const mapStateToProps = (state) => {
  return {
    quizs: getQuizs(state),
    isShow: state.quiz.isShow
  }
}

export default connect(mapStateToProps)(QuizList)
