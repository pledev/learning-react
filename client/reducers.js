import QuizReducer from './components/QuizReducer'
import QuizWidgetReducer from './components/QuizWidgetReducer'
import { combineReducers } from 'redux'

export default combineReducers({
  quiz: QuizReducer,
  widget: QuizWidgetReducer
})
