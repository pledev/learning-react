import React from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import QuizList from './components/QuizList'

const App = () => {
  return (
    <MuiThemeProvider>
    <QuizList/>
    </MuiThemeProvider>
  )
}

export default App
